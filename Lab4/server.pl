#!/usr/local/bin/perl -w
use strict;
use Data::Dumper;
use IO::Socket;
use Sys::Hostname;
print "Server is running on host: '" . hostname(), "'\n";
# you will have to figure out your hostname, print out the
# appropriate info to the user, read the lines from the client
# and print out the sorted results on your own- this just shows
# the reading of the two lines from the sample client above
my $port;
if ($ARGV[0]) {
  if( $ARGV[0] < 5000) {
    $port = 5000;
  } else {
  $port = $ARGV[0]; 
  }
} else {
  $port = 5000;
}
my $server = IO::Socket::INET->new(LocalPort => $port,
                                   Type => SOCK_STREAM,
                                   ReuseAddr => 1,
                                   Listen => 1) or die $@;


my $client = $server->accept();
my %client_data = ();
while (my $stuff = <$client>) {
  chomp $stuff;
  if ($stuff =~ /DONE/) {
    print "Client signaled termination\n";
    last;
  }
  $stuff =~ s/\s+$//;
  $stuff =~ /^(\w+): ([\d|\w]+)$/; 
  $client_data{ $1 } = $2;
}
print "A1.\n";
print "-----------------------------\n";
foreach(sort by_key keys %client_data) {
  print $_ . " => ". $client_data{$_} . "\n";
}
&wait_for_enter;
close($client);
close($server);
sub by_value_and_key {
  #print out a and b to see if getting bad values.
    ($client_data{$b} || 0) <=> ($client_data{$a} || 0)  # by descending count
        or
    $a cmp $b # else order by word
}
sub by_key {
    $a cmp $b  # by descending count
}
sub wait_for_enter {
 print "\nPress Enter";
 my $key = "x";
 until($key eq "\n") { $key = <STDIN>;}
}
