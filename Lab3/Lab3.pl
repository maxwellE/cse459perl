#!/usr/bin/perl -w
use strict;
# Maxwell Elliott Lab 3 CSE 459
if (($#ARGV+1) != 2) {
    print "Incorrect number of files provided! Please provide two files, your competitors text file and results text file\n\n";
    print "Example Command Line Call: './Lab3 competitors.txt output.txt'\n\n";
    exit 1;
}
my @competitors = ();
my @players = read_file($ARGV[0]);
if (@players < 2) {
  print "There are insufficent players to begin a battle, please add more competitors to your competitors text file.\n";
  exit 1;	
} else {
  my $outfile = $ARGV[1];
  open(RESULTS, "+>$outfile")or die $!;
	while (@players > 1) {
    my $loop_count = 0;
	  my $player1 = shift(@players);
    while ($loop_count < @players) {
       my $player2 = $players[$loop_count++];
       print RESULTS $player1->{Name} . " vs. " . $player2->{Name} . "\n";
       print RESULTS "-------------------------------\n";
       print RESULTS $player1->{Name} . "'s Stats:\n";
       unless(exists $player1->{HP}) {
         print RESULTS "HP: 40\n";
       }
       foreach my $key1 (keys %$player1) {
         next if $key1 eq "Name";
         print RESULTS "$key1: $player1->{$key1}\n"
       }
       print RESULTS $player2->{Name} . "'s Stats:\n";
       unless(exists $player2->{HP}) {
         print RESULTS "HP: 40\n";
       }
       foreach my $key1 (keys %$player2) {
         next if $key1 eq "Name";
         print RESULTS "$key1: $player2->{$key1}\n"
       }
       if(fight($player1, $player2) == 1) {
         print RESULTS "$player1->{Name} won the battle!\n";
       } else {
         print RESULTS "$player2->{Name} won the battle!\n";
       }
       print RESULTS "==================================\n";
    } 
  }
  print "All battles are complete!\n";
}
close RESULTS;
sub fight {
  my %p1 = %{(shift)};
  my %p2 = %{(shift)};
  unless (exists ($p1{HP})) {
    $p1{HP} = 40;
  }
  my $firstHP = $p1{HP};
  unless (exists ($p2{HP})) {
    $p2{HP} = 40;
  }
  my $secondHP = $p2{HP};
  print $p1{Name} . " vs. " . $p2{Name} . "\n";
  my $round_counter = 1;
  my $winner = 0;
  until( $winner > 0 ) {
    my $turn = int(rand(2))+1;
    print "Round " . $round_counter++ . "\n"; 
    print "Current Hit Points:    $p1{Name}: $p1{HP}    $p2{Name}: $p2{HP}\n";
    if ($turn == 1) {
      $winner = round(\%p1, \%p2);
    } else {
      $winner = round(\%p2, \%p1);
    }
  }
  $p1{HP} = $firstHP;
  $p2{HP} = $secondHP;
  return $winner;
}
sub round {
  my $first = (shift);
  my $second = (shift);
  if( successfull_atk($first->{POW}, $second->{DEF})) {
    my $hit1 = hit_points($first->{POW});
    print "$first->{Name} casts Magic Missile first, and hits for $hit1 of damage.\n";
        $second->{HP} -= $hit1; 
        if ($second->{HP} < 1) {
          print "$first->{Name} has won the battle\n";
          return 1;
        }
      }
      else {
        print "$first->{Name} casts Magic Missile first, and misses.\n";
      }
  if(successfull_atk($second->{POW}, $first->{DEF})) {
    my $hit2 = hit_points($second->{POW});
    print "$second->{Name} casts Magic Missile, and hits for $hit2 of damage.\n";
    $first->{HP} -= $hit2; 
        if ($first->{HP} < 1) {
          print "$second->{Name} has won the battle\n";
          return 2;
        }
    } else {
        print "$second->{Name} casts Magic Missile, and misses.\n";
      }
      my $key = "x";
      until($key eq "\n") { $key = <STDIN>;}
      return 0;
} 
sub successfull_atk {
  my $pow = (shift);
  my $def = (shift);
  my $result = 0;
  if (( $pow + int(rand(10))+1 - $def )> 0) {
    $result = 1;
  }
  return $result;
}
sub hit_points {
  my $pow = (shift);
  return int(($pow + int(rand(5))+1)/4);
}
sub read_file {
  my @recordset = ();
  my $file = $_[0];
  open(COMPETITORS, "<$file") or die $!;
  my $line_text = "";
  while (<COMPETITORS>) {
    next if m/^#.+$|^$/;
    if (m/^Name: [\w ]+$/){
      unless (length($line_text) == 0) {
        push(@recordset, ($line_text));
        $line_text = "";
      }
    } 
   $line_text .= $_ unless length($_) == 0;
  }
  push(@recordset, ($line_text));
  foreach my $raw_data (@recordset) {
    my @data_ary = split('\n', $raw_data);
    my %data_set; 
    foreach my $val (@data_ary) {
      if ($val =~ /^(DEF|POW|HP): (\d+)\s*/) {
        $data_set{ $1 } = $2;
      } elsif ($val =~ /^(Name): ([\w ]+)\s*/) {
        $data_set{ $1 } = $2;
     } else {
       next;
     }
  }
    push(@competitors,{%data_set});
}
 my @final_competitors = ();
  foreach my $player (@competitors) {
    my %player_hash = %$player;
    unless (exists ($player_hash{DEF})) {
     print $player_hash{Name} . " does not have a valid DEF field, he/she will be removed from the final roster.\n"; 
     next;
    }
    unless (exists ($player_hash{POW})) {
     print $player_hash{Name} . " does not have a valid POW field, he/she will be removed from the final roster.\n"; 
     next;
    }
    push (@final_competitors, $player);
  }
  close COMPETITORS;
  return @final_competitors;
}

