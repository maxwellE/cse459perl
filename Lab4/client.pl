#!/usr/local/bin/perl -w
use strict;
use IO::Socket;
# note that you will have to do your command line processing,
# and your opening of the input file, and reading from it - this
# example code just sets up a socket to a hard-coded machine
# and port, and prints a test line to it
my $hostname;
my $port;
my $inputfile;
if (($#ARGV+1) ==  3) {
  $hostname = (shift);
  $port = (shift);
  $inputfile = (shift);
} else{
  print "Incorrect number of command line arguments! Make sure to send 3 arguments as stated in the Lab 4 requirments.\n";
  exit;
}
open(DATA, $inputfile)or die $!;

my $s = IO::Socket::INET->new("$hostname:$port") or die $@;
# set up autoflushing
select $s;
$|++;
while(<DATA>) {
  chomp;
  my $line = $_;  
  if ($line =~ /^\w+: [\d|\w]+$/) {
    print $s $line . "\n" 
  }
}
print $s "DONE\n";
close ($s);
