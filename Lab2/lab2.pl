#!/usr/bin/perl -w
use strict;

#Maxwell Elliott

# using this <> in this manner will treat all of the files mentioned
# on the command line as one big textfile, and will read from them
# in turn
my %word_hash = ();
my @special_words = ();
my %special_hash = ();
while (<>) {
  chomp;
  push @special_words, /^#(\w+)/;
  my($line) = $_;
  
  my @words = split /\W+/,$line;
  foreach (@words) {
    next if $_ eq "";
    $word_hash{$_} += 1; 
  }
}
foreach my $key (@special_words) {
    if(not exists $special_hash{$key}) {
	$special_hash{$key} += $word_hash{$key};
	}
}
 while ( my ($key, $value) = each(%word_hash) ) {
    if($value < 10) {
        delete $word_hash{$key};
    }
}
my $size = keys %word_hash;

if ($size > 0) { 
    print "All words with occurences greater than or equal to 10 (sorted descending by count, if counts match then by ascii-betical):\n";
    print "Word      =>\t Count\n";
    print "======================\n";
    my $substr = "";
    foreach (sort by_count_and_name keys %word_hash) {
		 $substr = $_;
      while(length($substr) < 10 ) {
		$substr = $substr. " ";
    }
		print "$substr=>\t $word_hash{$_}\n";  
}
} else {
    print "There are no words in the provided file(s) that appear 10 or more times!\n";
    }
$size = keys %special_hash;
if ($size > 0) {
    print "\nAll special words with their counts (sorted by count descending, if match then by ascii-betical):\n";
      print "Word      =>\t Count\n";
    print "======================\n";
    my $substr = "";
    foreach (sort by_count_and_name keys %special_hash) {
		 $substr = $_;
      while(length($substr) < 10 ) {
		$substr = $substr. " ";
    }
		print "$substr=>\t $word_hash{$_}\n";  
    }
}

sub by_count_and_name {
  #print out a and b to see if getting bad values.
    ($word_hash{$b} || 0) <=> ($word_hash{$a} || 0)  # by descending count
        or
    $a cmp $b # else order by word
}
